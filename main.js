/**
 * Created by v.stokolosa on 2/23/15.
 */
/*jslint browser: true, plusplus: true, node: true */

'use strict';

function SquaresGenerator() {
    // data
    this.square = document.getElementsByClassName('someClass');
    this.len = this.square.length;
}

var app = new SquaresGenerator();

SquaresGenerator.prototype.getClicks = function (before, after, digit) {
    var target,
        oldValue,
        newValue,
        i;

    function clickHandler(event) {
        event = event || window.event;
        target = event.target || event.srcElement;

        if (target.hasAttribute('data-clicks')) {
            oldValue = target.dataset.clicks;
            target.dataset.clicks = +oldValue + digit;
        } else {
            target.setAttribute('data-clicks', 1);
        }

        if (after) {
            newValue = target.dataset.clicks;
            target.innerHTML = +newValue;

            app.bg(target);
        }
    }

    // clicks before & after generate
    if (before) {
        for (i = 0; i < this.len; i++) {
            this.square[i].addEventListener('click', clickHandler, false);
        }
    }
};

SquaresGenerator.prototype.generateClick = function () {
    var randomSquare,
        oldValue,
        i;

    for (i = 0; i < 100; i++) {
        randomSquare = Math.floor((Math.random() * 99) + 1);

        if (this.square[randomSquare].hasAttribute('data-clicks')) {
            oldValue = this.square[randomSquare].dataset.clicks;

            this.square[randomSquare].dataset.clicks = +oldValue + 1;
        } else {
            this.square[randomSquare].setAttribute('data-clicks', 1);
        }
    }
};

SquaresGenerator.prototype.showResult = function () {
    var result,
        i;

    for (i = 0; i < this.len; i++) {
        if (this.square[i].hasAttribute('data-clicks')) {
            result = this.square[i].dataset.clicks;

            this.square[i].innerHTML = +result;

            app.bg(this.square[i]);
        } else {
            this.square[i].innerHTML = 0;
        }
    }

    app.getClicks(true, true, 0);
};

SquaresGenerator.prototype.reset = function () {
    var i;

    for (i = 0; i < this.len; i++) {
        this.square[i].removeAttribute('data-clicks');
        this.square[i].innerHTML = '';
        this.square[i].style.background = '#FFF';
    }
};

SquaresGenerator.prototype.bg = function (square) {
    var result = +square.dataset.clicks;

    if (result <= 25) {
        square.style.background = '#FFF';
    } else if (result > 25 && result <= 50) {
        square.style.background = '#FCF6A9';
    } else if (result > 50 && result <= 75) {
        square.style.background = '#FCCF05';
    } else if (result > 75 && result <= 100) {
        square.style.background = '#FC8505';
    } else if (result > 100) {
        square.style.background = '#F50202';
    }
};

// calls
app.getClicks(true, false, 1);
