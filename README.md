###OOP implementation interesting task for Squares Generator.

###Task:
	1. Click on "Generate click" button have to generate 100 clicks for random squares;
	2. Click on "Show result" button have to:
		- show how many clicks are on the each square (e.g. text inside square);
		- for squares with clicks > 100 background should be -  #F50202;
		- clicks from 75 to 100 - #FC8505;
		- clicks from 50 to 75 - #FCCF05;
		- clicks from 25 to 50 - #FCF6A9;
		- clicks from 0 tо 25 - #FFF;
	3. Click on  "Reset" button have to reset all events and delete all text inside squares;
	4. Also squares have to save clicks before and after click to "Generate click" button and sum them each others.